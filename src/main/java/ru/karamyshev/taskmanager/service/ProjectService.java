package ru.karamyshev.taskmanager.service;

import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.exception.empty.EmptyUserIdException;
import ru.karamyshev.taskmanager.exception.empty.IdEmptyException;
import ru.karamyshev.taskmanager.exception.IndexIncorrectException;
import ru.karamyshev.taskmanager.exception.empty.NameEmptyException;
import ru.karamyshev.taskmanager.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository iProjectRepository;

    public ProjectService( final IProjectRepository iProjectRepository) {
        this.iProjectRepository = iProjectRepository;
    }

    @Override
    public void create(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Project project = new Project();
        project.setName(name);
        iProjectRepository.add(userId, project);
    }

    @Override
    public void create(String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        iProjectRepository.add(userId, project);
    }

    @Override
    public void add(String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        iProjectRepository.add(userId, project);
    }

    @Override
    public void remove(String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        iProjectRepository.remove(userId, project);
    }

    @Override
    public List<Project> findAll(String userId ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return iProjectRepository.findAll(userId);
    }

    @Override
    public void clear(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        iProjectRepository.clear(userId);
    }

    @Override
    public Project findOneByIndex(String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index <= 0) throw new IndexIncorrectException();
        return iProjectRepository.findOneByIndex(userId,index -1);
    }

    @Override
    public List<Project> findOneByName(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return iProjectRepository.findOneByName(userId, name);
    }

    @Override
    public Project updateProjectById(String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(Long.parseLong(id));
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeOneByIndex(String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index <= 0) throw new IndexIncorrectException();
        return iProjectRepository.removeOneByIndex(userId,index -1);
    }

    @Override
    public List<Project> removeOneByName(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return iProjectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project findOneById(String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return iProjectRepository.findOneById(userId, id);
    }

    @Override
    public Project removeOneById(String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return iProjectRepository.removeOneById(userId, id);
    }

    @Override
    public Project updateProjectByIndex(String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }
}
