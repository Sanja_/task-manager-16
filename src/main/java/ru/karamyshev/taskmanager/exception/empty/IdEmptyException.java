package ru.karamyshev.taskmanager.exception.empty;

public class IdEmptyException extends RuntimeException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }
}
