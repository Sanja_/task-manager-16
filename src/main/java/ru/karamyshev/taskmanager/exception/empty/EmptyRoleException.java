package ru.karamyshev.taskmanager.exception.empty;

public class EmptyRoleException extends RuntimeException {

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }
}
