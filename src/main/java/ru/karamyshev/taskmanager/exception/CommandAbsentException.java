package ru.karamyshev.taskmanager.exception;

public class CommandAbsentException extends RuntimeException{

    public CommandAbsentException() {

        super("Error! Index is  incorrect...");
    }
}
