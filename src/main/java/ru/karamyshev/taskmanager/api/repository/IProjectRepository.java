package ru.karamyshev.taskmanager.api.repository;

import ru.karamyshev.taskmanager.model.Project;
import java.util.List;

public interface IProjectRepository {

    void add(String userId,Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneById(String userId, String id);

    Project removeOneById(String userId, String id);

    Project findOneByIndex(String userId, Integer index);

    List<Project> findOneByName(String userId, String name);

    Project removeOneByIndex(String userId, Integer index);

    List<Project> removeOneByName(String userId, String name);
}
