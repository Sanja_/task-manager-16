package ru.karamyshev.taskmanager.api.repository;

import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

public interface ICommandRepository {

    List<AbstractCommand> getTerminalCommands();

}
