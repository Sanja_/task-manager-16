package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.model.Task;

import java.util.List;

public interface ITaskService {
    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findAll(String userId);

    void clear(String userId) ;

    Task findOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    List<Task> findOneByName(String userId, String name);

    Task removeOneById(String userId, String id);

    Task removeOneByIndex(String userId, Integer index);

    List<Task> removeOneByName(String userId, String name) ;

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

}
