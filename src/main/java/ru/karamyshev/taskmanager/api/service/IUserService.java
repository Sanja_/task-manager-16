package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.model.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User lockUserByLogin(String currentLogin, String login);

    User unlockUserByLogin(String login);

    User removeUserByLogin(String currentLogin, String login);

}
