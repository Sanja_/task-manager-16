package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

public interface ICommandService {

    List<AbstractCommand> getTerminalCommands();

}
