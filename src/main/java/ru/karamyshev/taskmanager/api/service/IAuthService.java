package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.model.User;

public interface IAuthService {

    String getUserId();

    void checkRoles(Role[] roles);

    String getCurrentLogin();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    void registry(String login, String password, String email);

    void renameLogin(String userId, String currentLogin, String newLogin);

    User showProfile(String userId, String login);

    void renamePassword(
            String userId,
            String currentLogin,
            String oldPassword,
            String newPassword
    );
}
