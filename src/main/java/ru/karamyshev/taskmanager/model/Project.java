package ru.karamyshev.taskmanager.model;

public class Project extends AbstractEntitty {

    private String name = "default name project";

    private String description = "default description project";

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }
}
