package ru.karamyshev.taskmanager.repository;

import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(final String userId, final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(final String userId,  final Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll(final String userId){
        final List<Project> result = new ArrayList<>();
        for (final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        for (final Project project: projects){
            if (userId.equals(project.getUserId()) &&
                    Long.parseLong(id) == project.getId()) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        final Project task = findOneById(userId, id);
        if (task == null) return null;
        projects.remove(task);
        return task;
    }

    @Override
    public Project findOneByIndex(String userId, final Integer index) {
        final int arraySize = projects.size();
        int iteration = 0;
        for (final Project project: projects){
            if (userId.equals(project.getUserId())){
                if (index <= arraySize && iteration == index) return projects.get(index);
            }
            iteration++;
        }
        return null;
    }

    @Override
    public List<Project> findOneByName(String userId, final String name) {
        final List<Project> projectsSample = new ArrayList<>();
        for (final Project project: projects){
            if (userId.equals(project.getUserId())){
                if (name.equals(project.getName())) projectsSample.add(project);
            }
        }
        return projectsSample;
    }

    @Override
    public Project removeOneByIndex(String userId, final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public List<Project> removeOneByName(String userId, final String name) {
        final List<Project> project = findOneByName(userId, name);
        if (project == null) return null;
        for( Project proj: project){
            remove(userId, proj);
        }

        return project;
    }
}
