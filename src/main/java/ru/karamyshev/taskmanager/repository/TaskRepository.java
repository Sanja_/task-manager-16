package ru.karamyshev.taskmanager.repository;

import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Task> task = findAll(userId);
        this.tasks.removeAll(task);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        for (final Task task: tasks){
            if (userId.equals(task.getUserId())) {
                if (Long.parseLong(id) == task.getId()) return task;
            }
        }
        return null;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        final int arraySize = tasks.size();
        int iteration = 0;
        for (final Task task: tasks){
            if (userId.equals(task.getUserId())){
                if (index <= arraySize && iteration == index) return tasks.get(index);
            }
            iteration++;
        }
        return null;
    }

    @Override
    public List<Task> findOneByName(final String userId, final String name) {
        final List<Task> projectsSample = new ArrayList<>();
        for (final Task task: tasks){
            if (userId.equals(task.getUserId())){
                if (name.equals(task.getName())) projectsSample.add(task);
            }
        }
        return projectsSample;
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public List<Task> removeOneByName(final String userId, final String name) {
        final List<Task> tasks = findOneByName(userId, name);
        if (tasks == null) return null;
        for( Task task: tasks){
            remove(userId, task);
        }
        return tasks;
    }
}
