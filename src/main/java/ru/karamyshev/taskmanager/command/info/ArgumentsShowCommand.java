package ru.karamyshev.taskmanager.command.info;

import ru.karamyshev.taskmanager.api.service.ICommandService;
import ru.karamyshev.taskmanager.command.AbstractCommand;


import java.util.List;

public class ArgumentsShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-agr";
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        System.out.println("\n [ARGUMENTS]");
        final ICommandService commandService = serviceLocator.getCommandService();
        final List<AbstractCommand> commandsList = commandService.getTerminalCommands();
        for (final AbstractCommand command : commandsList) System.out.println(command.arg());
    }
}
