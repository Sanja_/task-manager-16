package ru.karamyshev.taskmanager.command.info;

import ru.karamyshev.taskmanager.api.service.ICommandService;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.List;

public class CommandsShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        System.out.println("\n [COMMANDS]");
        final ICommandService commandService = serviceLocator.getCommandService();
        final List<AbstractCommand> commandsList = commandService.getTerminalCommands();
        for (final AbstractCommand command : commandsList) System.out.println(command.name());
    }

}
