package ru.karamyshev.taskmanager.command;

import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.enumerated.Role;

public abstract class AbstractCommand {

  protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator IServiceLocator) {
        this.serviceLocator = IServiceLocator;
    }

    public Role[] roles(){
      return null;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute();

}
