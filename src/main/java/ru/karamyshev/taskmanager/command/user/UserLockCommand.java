package ru.karamyshev.taskmanager.command.user;

import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {
    @Override
    public String arg() {
        return "-lckusr";
    }

    @Override
    public String name() {
        return "lock-user";
    }

    @Override
    public String description() {
        return "Locked users";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        final String currentLogin = serviceLocator.getAuthService().getCurrentLogin();
        serviceLocator.getUserService().lockUserByLogin(currentLogin, login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
