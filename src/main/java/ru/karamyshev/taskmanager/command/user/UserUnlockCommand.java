package ru.karamyshev.taskmanager.command.user;

import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UserUnlockCommand extends AbstractCommand {
    @Override
    public String arg() {
        return "-unlckusr";
    }

    @Override
    public String name() {
        return "unlock-user";
    }

    @Override
    public String description() {
        return "unlocked users";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{ Role.ADMIN};
    }
}
