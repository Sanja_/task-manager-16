package ru.karamyshev.taskmanager.command.user;

import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.model.User;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UserRemoveCommand extends AbstractCommand {
    @Override
    public String arg() {
        return "-rmvusr";
    }

    @Override
    public String name() {
        return "remove-user";
    }

    @Override
    public String description() {
        return "Remove users";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        final String currentLogin = serviceLocator.getAuthService().getCurrentLogin();
        serviceLocator.getUserService().removeUserByLogin(currentLogin, login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
