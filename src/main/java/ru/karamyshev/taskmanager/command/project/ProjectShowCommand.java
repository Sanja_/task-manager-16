package ru.karamyshev.taskmanager.command.project;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.model.Project;

import java.util.List;

public class ProjectShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-prlst";
    }

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        IAuthService authService = serviceLocator.getAuthService();
        IProjectService projectService = serviceLocator.getProjectService();
        final String userId = authService.getUserId();
        final List<Project> projects = projectService.findAll(userId);
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }
}
