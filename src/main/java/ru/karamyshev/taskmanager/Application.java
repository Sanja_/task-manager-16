package ru.karamyshev.taskmanager;

import ru.karamyshev.taskmanager.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
